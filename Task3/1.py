def validate_point(x: float, y: float) -> bool:
    A = {x: -1, y: 1}
    B = {x: 1, y: 1}
    C = {x: 0, y: 0}
    axe1 = (A[x] - x) * (B[y] - A[y]) - (B[x] - A[x]) * (A[y] - y)
    axe2 = (B[x] - x) * (C[y] - B[y]) - (C[x] - B[x]) * (B[y] - y)
    axe3 = (C[x] - x) * (A[y] - C[y]) - (A[x] - C[x]) * (C[y] - y)

    if (axe1 > 0 and axe2 > 0 and axe3 > 0
            or axe1 < 0 and axe2 < 0 and axe3 < 0):
        return True

    return False


if __name__ == '__main__':
    x = float(input('enter number'))
    y = float(input('enter number'))
    print(f'is point in shadow area: {validate_point(x, y)}')
