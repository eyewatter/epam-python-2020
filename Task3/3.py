CARDS = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A', 'Joker']


def play(first_card: str, second_card: str) -> str:
    if first_card not in CARDS or second_card not in CARDS:
        return 'value error'
    try:
        first_card = int(first_card)
    except ValueError:
        if first_card == 'Joker':
            return 'Do not cheat!'
        if first_card == 'A':
            first_card = 1
        else:
            first_card = 0

    try:
        second_card = int(second_card)
    except ValueError:
        if second_card == 'Joker':
            return 'Do not cheat!'
        if second_card == 'A':
            second_card = 1
        else:
            second_card = 0

    return str((first_card + second_card) % 10)


if __name__ == '__main__':
    first_card = str(input('play first card'))
    second_card = str(input('play second card'))
    print(
        'your result:', play(first_card, second_card)
    )