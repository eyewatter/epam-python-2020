<H1>Task #1 </H1>

Write a program that determines whether the Point A(x,y) is in the shaded area or not.

<H1>Solution</H1>

<H4>Code</H4>

```python
def validate_point(x: float, y: float) -> bool:
    A = {x: -1, y: 1}
    B = {x: 1, y: 1}
    C = {x: 0, y: 0}
    axe1 = (A[x] - x) * (B[y] - A[y]) - (B[x] - A[x]) * (A[y] - y)
    axe2 = (B[x] - x) * (C[y] - B[y]) - (C[x] - B[x]) * (B[y] - y)
    axe3 = (C[x] - x) * (A[y] - C[y]) - (A[x] - C[x]) * (C[y] - y)

    if (axe1 > 0 and axe2 > 0 and axe3 > 0
            or axe1 < 0 and axe2 < 0 and axe3 < 0):
        return True
    return False


if __name__ == '__main__':
    x = float(input('enter number'))
    y = float(input('enter number'))
    print(f'is point in shadow area: {validate_point(x, y)}')
```
<H4>Output</H4>

```diff 
enter number 0.6
enter number 0.7
is point in shadow area: True
```

<H1>Task #2 </H1>

Write a program that prints the input number from 1 to 100. 
But for multiple of three print "Fizz" instead of the number and for the multiples 
of five print "Buzz". 
For numbers which are multiples of both three and five print "FizzBuzz".

<H1>Solution</H1>

<H4>Code</H4>

```python
def is_multiple_3_or_5(number: int) -> str:
    if number not in range(1, 100):
        return f"{number}not in range [1, 100]"
    if number % 3 == 0 and number % 5 == 0:
        return f"{str(number)} FizzBuzz"
    elif number % 3 == 0:
        return f"{str(number)} Fizz"
    elif number % 5 == 0:
        return f"{str(number)} Buzz"
    else:
        return str(number)


if __name__ == '__main__':
    number = int(input('enter number'))
    print(is_multiple_3_or_5(number))


```
<H4>Output</H4>

```diff 
enter number45
45 FizzBuzz
```

<H1>Task #3 </H1>

 Simulate the one round of play for baccarat game:
Rules:


cards have a point value:

the 2 through 9 cards in each suit are worth face value (in points);
the 10, jack, queen and king have no point value (i.e. are worth zero);
aces are worth 1 point


Sum the values of cards. If total is more than 9 reduce 10 from result. 
E.g. 5 + 9 = 14, 14 - 10 = 4, 4 is the result


<H1>Solution</H1>

<H4>Code</H4>

```python
CARDS = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A', 'Joker']


def play(first_card: str, second_card: str) -> str:
    if first_card not in CARDS or second_card not in CARDS:
        return 'value error'
    try:
        first_card = int(first_card)
    except ValueError:
        if first_card == 'Joker':
            return 'Do not cheat!'
        if first_card == 'A':
            first_card = 1
        else:
            first_card = 0

    try:
        second_card = int(second_card)
    except ValueError:
        if second_card == 'Joker':
            return 'Do not cheat!'
        if second_card == 'A':
            second_card = 1
        else:
            second_card = 0

    return str((first_card + second_card) % 10)


if __name__ == '__main__':
    first_card = str(input('play first card'))
    second_card = str(input('play second card'))
    print(
        'your result:', play(first_card, second_card)
    )
```

<H4>Output</H4>

```diff 
play first card 7
play second card J
your result: 7
```
