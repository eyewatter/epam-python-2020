def is_multiple_3_or_5(number: int) -> str:
    if number not in range(1, 100):
        return f"{number}not in range [1, 100]"
    if number % 3 == 0 and number % 5 == 0:
        return f"{str(number)} FizzBuzz"
    elif number % 3 == 0:
        return f"{str(number)} Fizz"
    elif number % 5 == 0:
        return f"{str(number)} Buzz"
    else:
        return str(number)


if __name__ == '__main__':
    number = int(input('enter number'))
    print(is_multiple_3_or_5(number))
