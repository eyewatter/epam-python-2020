<H1>Task #1 </H1>

 Create function IsSorted, determining whether a given array of 
 integer values of arbitrary length is sorted in a given order 
 (the order is set up by enum value SortOrder). 
 Array and sort order are passed by parameters. 
 Function does not change the array

<H1>Solution</H1>

<H4>[link to code](1.py)</H4>

<H4>Output</H4>

```diff 
list [9, 7, 3, 2, 2] is sorted by SortOrder.descending: True 
```


<H1>Task #2 </H1>

Create function Transform, replacing the value of each element of an 
integer array with the sum of this element value and its index, only if 
the given array is sorted in the given order (the order is set up by enum 
value SortOrder). Array and sort order are passed by parameters. To check,
if the array is sorted, the function IsSorted from the Task 1 is called.

<H1>Solution</H1>

<H4>[link to code](2.py)</H4>

<H4>Output</H4>

```diff 
new value is [3, 5, 7]
```


<H1>Task #3 </H1>

Create function MultArithmeticElements,
 which determines the multiplication of the first n elements 
 of arithmetic progression of real numbers with a given initial 
 element of progression a1 and progression step t. 
an is calculated by the formula (antl = an +1).

<H4>[link to code](3.py)</H4>

<H4>Output</H4>

```diff 
multiplication of elements: start 5, step 3 and amount 4 is 6160
```

<H1>Task #4 </H1>

Create function SumGeometricElements, determining the sum 
of the first elements of a decreasing geometric progression 
of real numbers with a given initial element of a progression 
aj and a given progression step t, while the last element must 
be greater than a given alim. 
an is calculated by the formula (an+1 = an * t), 0<t<l.

<H4>[link to code](4.py)</H4>

<H4>Output</H4>

```diff 
sum of elements: start 100, step 0.5 and minimal value 20 is 175.0
```

