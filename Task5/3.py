import operator
from functools import reduce


def mult_arithmetic_elements(initial: int, step: (int, float), count: int, ) -> (int, float):
    def progression(_initial, _step):
        while True:
            _initial += _step
            yield _initial

    p = progression(initial, step)
    return reduce(operator.mul, [next(p) for i in range(count - 1)], 1) * initial


if __name__ == '__main__':
    start = 5
    step = 3
    count = 4
    print(f'multiplication of elements: start {start}, step {step} and amount {count}'
          f' is {mult_arithmetic_elements(start, step, count)}')
