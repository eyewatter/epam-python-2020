import enum


class SortOrder(enum.Enum):
    ascending = 1
    descending = -1


def is_sorted(array: list, order: enum) -> bool:

    if all(array[i] >= array[i + 1] for i in range(len(array) - 1)) and order == SortOrder.descending:
        return True
    if all(array[i] <= array[i + 1] for i in range(len(array) - 1)) and order == SortOrder.ascending:
        return True
    return False


if __name__ == '__main__':
    _list = [9, 7, 3, 2, 2]
    method = SortOrder.descending
    print(f'list {_list} is sorted by {method}: {is_sorted(_list, method)}')
