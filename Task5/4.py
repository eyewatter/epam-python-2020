import operator
from functools import reduce


def sum_geometric_elements(initial: int, step: float, alim: (int, float)) -> (int, float):
    def progression(_initial, _step):
        while True:
            _initial *= _step
            yield _initial

    if 0 > step < 1:
        print('error value')
        return -1

    p = progression(initial, step)
    result = 0
    while initial >= alim:
        result += initial
        initial *= step
    return result


if __name__ == '__main__':
    start = 100
    step = 0.5
    min_value = 20
    print(f'sum of elements: start {start}, step {step} and minimal value {min_value}'
          f' is {sum_geometric_elements(start, step, min_value)}')
