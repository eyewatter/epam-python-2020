import enum


class SortOrder(enum.Enum):
    ascending = 1
    descending = -1


def transform(array: list, order: enum):
    # if array is sorted correctly
    if all(array[i] >= array[i + 1] for i in range(len(array) - 1)) and order == SortOrder.descending \
            or all(array[i] <= array[i + 1] for i in range(len(array) - 1)) and order == SortOrder.ascending:
        return [array[i] + i for i in range(len(array))]

    return array


if __name__ == '__main__':
    print(f'new value is {transform([3, 4, 5], SortOrder.ascending)}')
