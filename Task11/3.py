def remember_result(func):
    last = "None"

    def wrapper(*args, **kwargs):
        nonlocal last
        print(f'last: {last}')
        last = func(*args, **kwargs)
        return last

    return wrapper


def call_once(func):
    value = None

    def wrapper(a, b):
        nonlocal value
        if value is None:
            value = func(a, b)

        return value
    return wrapper


@call_once
def sum_of_numbers(a, b):
    return a + b


if __name__ == '__main__':
    print(sum_of_numbers(13, 42))
    print(sum_of_numbers(999, 100))
    print(sum_of_numbers(134, 412))
    print(sum_of_numbers(856, 232))