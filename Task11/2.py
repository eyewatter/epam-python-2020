def remember_result(func):
    last = "None"
    current = any

    def wrapper(*args):
        nonlocal current
        nonlocal last
        print(f'last result: {last}')
        last = func(*args)
        print('_'*10)
    return wrapper


@remember_result
def sum_list(*args):
    result = ""
    for item in args:
        result += str(item)
    print(f"Current result = '{result}'")
    return result


if __name__ == '__main__':
    sum_list('a', 'b')
    sum_list('abc', 'cde')
