<H1>Task #1</H1>

![Src](1.PNG)

<H1>Solution</H1>

<H4>[link to task 1 code](1.py)</H4>

<H4>Output</H4>

```diff 
start a value: I am global variable!
changed value :
I am local variable!
```

<H4>[link to task 1.1 code](1.py)</H4>

<H4>Output</H4>

```diff 
global a value:
I am global variable!
```

<H4>[link to task 1.2 code](1.py)</H4>

<H4>Output</H4>

```diff 
local a value:
I am variable from enclosed function!
```

<H1>Task #2</H1>

![Src](2.PNG)

<H1>Solution</H1>

<H4>[link to code](2.py)</H4>

<H4>Output</H4>

```diff 
last result: None
Current result = 'ab'
__________
last result: ab
Current result = 'abccde'
__________
```

![Src](3.PNG)

<H1>Solution</H1>

<H4>[link to code](3.py)</H4>

<H4>Output</H4>

```diff 
55
55
55
55
```

<H1>Task 4 Explanation</H1>
App output is always '5' because firstly the mod_c module initializes and its variable 
overwrites by mod_b module. if you remove the import of module mod_b from module mod_a, then the
result will be the same as when initializing module mod_c