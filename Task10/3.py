import pandas as pd


def sort_student_by_mark(file_path, number_of_top_students=5):
    data = pd.read_csv(file_path)

    data = data.sort_values(by=['average mark'], ascending=False)

    try:
        ret_val = [data['student name'][i] for i in range(number_of_top_students)]
    except KeyError as e:
        print('WARN: number_of_words is out of range')
        ret_val = [data['student name'][i] for i in range(len(data))]
    return ret_val


def sort_students_by_age(file_path):
    data = pd.read_csv(file_path)

    data = data.sort_values(by=['age'], ascending=False)

    data.to_csv('sorted_ages.csv')



if __name__ == '__main__':
    print(sort_student_by_mark('students.csv'))
    print(sort_student_by_mark('students.csv', 100000))
    sort_students_by_age('students.csv')
