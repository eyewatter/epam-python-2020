def read_file(filename: str):
    with open(filename, 'r') as file:
        data = file.read()
        return data


def most_common_words(filepath, number_of_words=3):
    global ret_val
    data = read_file(filepath)
    data = data.lower()
    data = ''.join(char for char in data if char.isalnum() or char == ' ').split(' ')

    frequency = {}
    for word in data:
        if word in frequency:
            frequency[word] += 1
        else:
            frequency[word] = 0

    sort = sorted(frequency.items(), key=lambda x: x[1], reverse=True)
    try:
        ret_val = [sort[i][0] for i in range(number_of_words)]
    except IndexError:
        print('WARN: number_of_words is out of range')
        ret_val = [sort[i][0] for i in range(len(sort))]
    return ret_val


if __name__ == '__main__':
    print(most_common_words('lorem_ipsum.txt', 3))
    print(most_common_words('lorem_ipsum.txt', 1000))
