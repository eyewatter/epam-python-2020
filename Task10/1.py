def read_student_names(filename):
    with open(filename, 'r')as file:
        data = file.read()

    data = data.split('\n')

    return data


def write_file(filename: str, data):
    with open(filename, 'w') as file:
        for word in data:
            file.write(word + '\n')


if __name__ == '__main__':
    names = read_student_names('unsorted_names.txt')
    write_file('sorted_names.txt', names)
