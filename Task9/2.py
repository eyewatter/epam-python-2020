def intersect_all(*strings):
    string = ""
    for i in strings:
        string += "".join(i)
    string = set(string)

    for i in strings:
        for j in i:
            string = string.intersection(j)

    return string


def make_set(*strings):
    string = ""
    for i in strings:
        string += "".join(i)
    string = set(string)

    return string


def symmetric_difference(*strings):
    letters = set("abcdefghijklmnopqrstuwxyz")
    string = ""
    for i in strings:
        string += "".join(i)
    string = set(string)
    letters = letters.symmetric_difference(string)
    return letters


def intersect_two(*strings):
    main_string = ""
    for i in strings:
        main_string += "".join(i)
    ret_val = ""
    for string in strings:
        for words in string:
            for i in words:
                count_current = words.count(i)
                count_global = main_string.count(i)
                if count_global - count_current > 0:
                    ret_val += i

    return set(ret_val)


if __name__ == '__main__':

    test_strings = ['one', 'second', 'five']

    print(f'intersection of all letters: {intersect_all(test_strings)}')

    print(f'set of all elements: {make_set(test_strings)}')

    print(f'symmetric difference: {symmetric_difference(test_strings)}')

    print(f'two time occurrence: {intersect_two(test_strings)}')
