import re


def swap_quotes(string: str):
    string = string.replace('\"', 'tmp_string')
    string = string.replace('\'', '\"')
    string = string.replace('tmp_string', '\'')
    return string


def is_palindrome(string: str):
    string = re.sub('\'|,|!|\.|\?|"|', '', string)
    string = string.lower()

    # Follow the words, not the letters:
    temp = string.split(" ")

    if temp[:] == temp[::-1]:
        return True

    string = string.replace(" ", "")

    if string[:] == string[::-1]:
        return True
    return False


def get_shortest_word(string: str) -> str:
    length = 0
    min_length = len(string)
    word = ""
    string = re.sub('[\n\t.,?!]', ' ', string)

    for i in range(len(string)):
        if string[i] != " ":
            length += 1

        else:
            # if it is a shorter word and not a multiple spaces
            if min_length > length != 0:
                min_length = length
                word = string[i - length:i:]

            length = 0

    return word


if __name__ == '__main__':
    original_string = 'He "said" \'Hello\' to me'
    print(f'original string: {original_string}')
    print(f'swapped string: {swap_quotes(original_string)}')

    print('_' * 20)
    print('2 task')

    palindrome_string1 = 'Is it crazy how saying sentences backwards creates backwards ' \
                         'sentences saying how crazy it is?'
    print(f'string:{palindrome_string1}\t is palindrome: {is_palindrome(palindrome_string1)}')

    palindrome_string2 = 'Ed, I saw Harpo Marx ram Oprah W. aside.'
    print(f'string:{palindrome_string2}\t is palindrome: {is_palindrome(palindrome_string2)}')
    palindrome_string3 = 'some random text which is definitely not a palindrome'
    print(f'string:{palindrome_string3}\t is palindrome: {is_palindrome(palindrome_string3)}')

    print('_' * 20)
    print('3 task')

    task3_string_1 = 'Python is simple and effective!'
    task3_string_2 = 'Any a lot,     a?'

    print(f'shortest word in str: {task3_string_1}\t is "{get_shortest_word(task3_string_1)}"')
    print(f'shortest word in str: {task3_string_2}\t is "{get_shortest_word(task3_string_2)}"')
    print(f'shortest word in str: ""\t is "{get_shortest_word("")}"')


