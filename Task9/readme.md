<H1>Task #1 - #3 </H1>

![Src](1.jpg)


<H1>Solution</H1>

<H4>[link to code](1.py)</H4>

<H4>Output</H4>

```diff 
original string: He "said" 'Hello' to me
swapped string: He 'said' "Hello" to me
____________________
2 task
string:Is it crazy how saying sentences backwards creates backwards sentences saying how crazy it is?	 is palindrome: True
string:Ed, I saw Harpo Marx ram Oprah W. aside.	 is palindrome: True
string:some random text which is definitely not a palindrome	 is palindrome: False
____________________
3 task
shortest word in str: Python is simple and effective!	 is "is"
shortest word in str: Any a lot,     a?	 is "a"
shortest word in str: ""	 is ""
```


<H1>Task #4 </H1>

![Src](2.jpg)

<H1>Solution</H1>

<H4>[link to code](2.py)</H4>

<H4>Output</H4>

```diff 
intersection of all letters: {'e'}
set of all elements: {'f', 'd', 'n', 'i', 'o', 'v', 'e', 's', 'c'}
symmetric difference: {'y', 'a', 'r', 'k', 'v', 'h', 'u', 't', 'm', 'l', 'x', 'p', 'g', 'b', 'w', 'z', 'q', 'j'}
two time occurrence: {'e', 'o', 'n'}
```

<H1>Task #5 </H1>

![Src](3.jpg)

<H1>Solution</H1>

<H4>[link to code](3.py)</H4>

<H4>Output</H4>

```diff 
string is: some random text i wrote for test
frequency is: {'s': 2, 'o': 4, 'm': 2, 'e': 4, ' ': 6, 'r': 3, 'a': 1, 'n': 1, 'd': 1, 't': 5, 'x': 1, 'i': 1, 'w': 1, 'f': 1}
```
