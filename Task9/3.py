from collections import Counter


def count_letters(string: str):
    return dict(Counter(string))


if __name__ == '__main__':
    test_string = 'some random text i wrote for test'
    print(f'string is: {test_string}')
    print(f'frequency is: {count_letters(test_string)}')
