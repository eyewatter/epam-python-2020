<H1>Task</H1>

![Src](ex.bmp)

<H4>Task 1</H4>

```diff 
if a > b no error
else throws assertion error
```


<H4>Task #2 </H4>

```diff 
if there is an exception it falls
else prints:
    after bar
    or this after bar
```


<H4>Task #3 </H4>

```diff 
returns 2 
```

<H4>Task #4 </H4>

```diff 
returns 2 
```

<H4>Task #5 </H4>

```diff 
error has occurred
```


<H4>Task #6 </H4>

```diff 
if path exists it opens
else prints
    file not found
```
