<H1>Task #1 </H1>

Implement a function, that receives changeable number of dictionaries 
(keys - letters, values - numbers) and combines them into one dictionary.
Dict values should be summarized in case of identical keys


<H1>Solution</H1>

<H4>[link to code](1.py)</H4>

<H4>Output</H4>

```diff 
combine of dicts: {'a': 600, 'b': 200, 'c': 300, 'd': 100}
```


<H1>Task #2 </H1>

create generic type CustomList - the list of values, which has 
length that is extended when new elements are added to the list.

<H1>Solution</H1>

<H4>[link to code](2.py)</H4>

<H4>Output</H4>

```diff 
elements are: [3, 4, 'sda']  len is 3
removed 4 by value [3, 'sda']  len is 2
find by index lst[1]: 'sda'
print all:  [3, 'sda']
cleared list:  []
remove non-existent element 99 : no such element
```
