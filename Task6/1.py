from collections import Counter


def combine_dicts(*args:dict):
    main_dict = dict()
    for i in range(len(args)):
        main_dict = dict(args[i].items() + main_dict.items())
                         # [(k, args[i + 1][k] + main_dict[k]) for k in set(args[i + 1]) & set(main_dict)])


def combine_dicts(*args):
    rez = Counter()
    for i in args:
        i = Counter(i)
        rez += i
    return dict(rez)


if __name__ == '__main__':
    dict_1 = {'a': 100, 'b': 200}
    dict_2 = {'a': 200, 'c': 300}
    dict_3 = {'a': 300, 'd': 100}
    print(f'combine of dicts: {combine_dicts(dict_1, dict_2, dict_3)}')
