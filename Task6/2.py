class Node:

    def __init__(self, data=None, next=None, id=None):
        self.data = data
        self.next = next
        self.id = id

    def __repr__(self):
        return repr(self.data)


class LinkedList:
    def __init__(self):
        self.len = 0
        self.head = None

    def __repr__(self):
        nodes = []
        curr = self.head
        while curr:
            nodes.append(repr(curr))
            curr = curr.next
        return '[' + ', '.join(nodes) + ']'

    def append(self, data):
        if not self.head:
            self.head = Node(data=data, id=0)
            return

        last_id = 1
        curr = self.head
        while curr.next:
            last_id += 1
            curr = curr.next

        curr.next = Node(data=data, id=last_id)
        self.len += 1

    def clear(self):
        self.head = None
        self.len = 0
        return self

    def find(self, key):
        curr = self.head
        while curr and curr.data != key:
            curr = curr.next
        return curr

    def remove(self, key):
        curr = self.head
        prev = None
        while curr and curr.data != key:
            prev = curr
            curr = curr.next
        # Unlink it from the list
        try:
            if prev is None:
                self.head = curr.next
            elif curr:
                prev.next = curr.next
                curr.next = None

            nnext = prev.next
            while nnext:
                nnext.id -= 1
                nnext = nnext.next
            self.len -= 1
        except Exception:
            return 'no such element'
        return self

    def __getitem__(self, number: int):
        if not self.head:
            return []
        else:
            curr = self.head
            while curr:
                if curr.id == number:
                    return curr
                curr = curr.next
        raise IndexError('Bad index')

    def __len__(self):
        return self.len + 1


if __name__ == '__main__':
    lst = LinkedList()
    lst.append(3)
    lst.append(4)
    lst.append('sda')
    print(f'elements are: {lst}  len is {len(lst)}')
    print(f'removed 4 by value {lst.remove(4)}  len is {len(lst)}')
    print(f'find by index lst[1]: {lst[1]}')
    print(f'print all:  {lst}')
    print(f'cleared list:  {lst.clear()}')
    print(f'remove non-existent element 99 : {lst.remove(99)}')
