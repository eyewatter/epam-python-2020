def count_negtives(list):
    return "".join(map(str, list)).count("-")


if __name__ == '__main__':
    input_numbers = [4, -9, 8, -11, 8]
    print(f'There are {count_negtives(input_numbers)} negative numbers in list {input_numbers}')
