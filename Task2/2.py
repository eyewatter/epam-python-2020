def change_positions(players: list):
    return players[::-1]


if __name__ == '__main__':
    players = ['Ashleigh Barty', 'Simona Halep', 'Naomi Osaka', 'Karolina Pliskova', 'Elina Svitolina']
    print(f'original players: {players}')
    print(f'reversed players: {change_positions(players)}')
