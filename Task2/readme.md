<H1>Task #1 </H1>

Return the count of negative numbers in next list
[4, -9, 8, -11, 8] Note: do not use conditionals or loops

<H1>Solution</H1>

<H4>Code</H4>

```python
def count_negtives(list):
    return "".join(map(str, list)).count("-")


if __name__ == '__main__':
    input_numbers = [4, -9, 8, -11, 8]
    print(f'There are {count_negtives(input_numbers)} negative numbers in list {input_numbers}')
```
<H4>Output</H4>

```diff 
There are 2 negative numbers in list [4, -9, 8, -11, 8]
```

<H1>Task #2 </H1>

You have first 5 best tennis players according APT rankings.
Set the first-place player (at the front of the list) to last place and vice versa.

<H1>Solution</H1>

<H4>Code</H4>

```python
def change_positions(players: list):
    return players[::-1]


if __name__ == '__main__':
    players = ['Ashleigh Barty', 'Simona Halep', 'Naomi Osaka', 'Karolina Pliskova', 'Elina Svitolina']
    print(f'original players: {players}')
    print(f'reversed players: {change_positions(players)}')

```
<H4>Output</H4>

```diff 
original players: ['Ashleigh Barty', 'Simona Halep', 'Naomi Osaka', 'Karolina Pliskova', 'Elina Svitolina']
reversed players: ['Elina Svitolina', 'Karolina Pliskova', 'Naomi Osaka', 'Simona Halep', 'Ashleigh Barty']
```

<H1>Task #3 </H1>

 Swap words "reasonable" and "unreasonable" in quote "The reasonable man adapts himself to the world; the unreasonable one persists in trying to adapt the world to himself."
Note. Do not use <string>.replace() function or similar


<H1>Solution #1</H1>

<H4>Code</H4>

```python
def swap_words(string: str):
    string = string.split(" reasonable")
    string = "----".join(string)

    string = string.split(" unreasonable")
    string = " reasonable".join(string)

    string = string.split("----")
    string = " unreasonable".join(string)
    return string


if __name__ == '__main__':
    string = "The reasonable man adapts himself to the world;" \
             " the unreasonable one persists in trying to adapt the world to himself."
    print(string)
    print(swap_words(string))

```
<H4>Output</H4>

```diff 
The reasonable man adapts himself to the world; the unreasonable one persists in trying to adapt the world to himself.
The unreasonable man adapts himself to the world; the reasonable one persists in trying to adapt the world to himself.
```


<H2>Solution #2</H2>

<H4>Code</H4>

```python
import re

def change_positions(str1: str):
    list1 = re.split(r' |,', str1)
    word1, word2 = list1.index('reasonable'), list1.index('unreasonable')
    sw = list1[:]
    sw[word1:word1 + 1:], sw[word2:word2 + 1:] = sw[word2:word2 + 1:], sw[word1:word1 + 1:]
    return " ".join(sw)


if __name__ == '__main__':
    string = 'The reasonable man adapts himself to the world; ' \
             'the unreasonable one persists in trying to adapt the world to himself.'
    print(string)
    print(change_positions(string))

```
<H4>Output</H4>

```diff 
The reasonable man adapts himself to the world; the unreasonable one persists in trying to adapt the world to himself.
The unreasonable man adapts himself to the world; the reasonable one persists in trying to adapt the world to himself.
```
