import re


def change_positions(str1: str):
    list1 = re.split(r' |,', str1)
    word1, word2 = list1.index('reasonable'), list1.index('unreasonable')
    sw = list1[:]
    sw[word1:word1 + 1:], sw[word2:word2 + 1:] = sw[word2:word2 + 1:], sw[word1:word1 + 1:]
    return " ".join(sw)


if __name__ == '__main__':
    string = 'The reasonable man adapts himself to the world; ' \
             'the unreasonable one persists in trying to adapt the world to himself.'
    print(string)
    print(change_positions(string))
