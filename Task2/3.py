def swap_words(string: str):
    string = string.split(" reasonable")
    string = "----".join(string)

    string = string.split(" unreasonable")
    string = " reasonable".join(string)

    string = string.split("----")
    string = " unreasonable".join(string)
    return string


if __name__ == '__main__':
    string = "The reasonable man adapts himself to the world;" \
             " the unreasonable one persists in trying to adapt the world to himself."
    print(string)
    print(swap_words(string))
