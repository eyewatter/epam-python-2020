import sys


class Rectangle:
    minimal_difference = 0.000001

    def __init__(self, side_a: float, side_b: float = 5.0):
        self._side_a_ = side_a
        self._side_b_ = side_b

    def __repr__(self):
        return f'[{str(self._side_a_)}, {str(self._side_b_)}]'

    def get_side_a(self):
        return self._side_a_

    def get_side_b(self):
        return self._side_b_

    def area(self):
        return self._side_a_ * self._side_b_

    def perimeter(self):
        return 2 * (self._side_b_ * self._side_a_)

    def is_square(self):
        return abs(self._side_a_ - self._side_b_) < self.minimal_difference

    def replace_sides(self):
        self._side_b_, self._side_a_ = self._side_a_, self._side_b_


class ArrayRectangles:

    def __init__(self, *rectangles):
        self.array = list
        self._last_index_ = 0
        """
        if first parameter is int creates empty list. if all parameters are Rectangles

        """
        if type(rectangles[0] == int):
            self.array = [None] * rectangles[0]

        elif all(isinstance(item, Rectangle) for item in rectangles):
            for rectangle in rectangles:
                self._last_index_ += 1
                self.array.append(rectangle)

    def __repr__(self):
        return str(self.array)

    def add_rect(self, *rect) -> bool:
        try:
            self.array[self._last_index_] = Rectangle(rect[0], rect[1])
            self._last_index_ += 1
        except IndexError:
            return False
        else:
            return True

    def number_max_area(self):
        max_area = -1
        max_id = 0
        for i in range(len(self.array)):
            if max_area < self.array[i].area():
                max_area = self.array[i].area()
                max_id = i

        return max_id

    def number_min_perimeter(self):
        min_perimeter = sys.maxsize
        min_id = 0
        for i in range(len(self.array)):
            if min_perimeter > self.array[i].perimeter():
                min_perimeter = self.array[i].perimeter()
                min_id = i

        return min_id

    def number_squares(self):
        counter = 0
        for rect in self.array:
            if rect.is_square():
                counter += 1
        return counter


if __name__ == '__main__':
    # reserve for 3 rectangles
    rectangles = ArrayRectangles(3)

    # insert 3 rectangles
    rectangles.add_rect(4.0, 6.0)
    rectangles.add_rect(2.0, 6.0)
    rectangles.add_rect(30.0, 30.0)

    # try to insert one more
    print(f'try to insert one more: {rectangles.add_rect(30.0, 30.0)}')

    print(f'max area index: {rectangles.number_max_area()}')
    print(f'min perimeter index: {rectangles.number_min_perimeter()}')
    print(f'number of squares: {rectangles.number_squares()}')
