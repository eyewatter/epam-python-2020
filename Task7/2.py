import sys


class Employee:
    def __init__(self, name: str, salary: float, bonus=None):
        self._name_ = name
        self._bonus_ = bonus
        self._salary_ = salary

    def get_name(self):
        return self._name_

    def set_name(self, name):
        self._name_ = name

    def get_salary(self):
        return self._salary_

    def set_salary(self, salary):
        self._salary_ = salary

    def set_bonus(self, bonus):
        self._bonus_ = bonus

    def to_pay(self):
        return self._salary_ + self._bonus_

    def __repr__(self):
        return f'{[self._name_, self._salary_, self._bonus_]}'

    Name = property(get_name, set_name)
    Salary = property(get_salary, set_salary)


class SalesPerson(Employee):
    def __init__(self, name, salary, percent):
        super().__init__(name, salary)
        self.percent = percent

    def set_bonus(self, bonus):
        if super()._bonus_ is None:
            print('super bonus is not set')
        else:
            if self.percent > 200:
                super()._bonus_ *= 3
            elif self.percent > 100:
                super()._bonus_ *= 2


class Manager(Employee):
    def __init__(self, name: str, salary: float, quantity: int):
        super().__init__(name, salary)
        self._quantity = quantity

    def set_bonus(self, bonus):
        if super()._bonus_ is None:
            print('super bonus is not set')
        else:
            if self._quantity > 150:
                super()._bonus_ += 1000
            elif self._quantity > 100:
                super()._bonus_ += 500


class Company:
    def __init__(self, employee: list):
        self._employee = employee

    def give_everybody_bonus(self, bonus: int):
        for emp in self._employee:
            emp.set_bonus(bonus)

    def total_to_pay(self):
        return sum([emp.to_pay() for emp in self._employee])

    def name_max_salary(self):
        index = 0
        maxx = -sys.maxsize
        for i in range(len(self._employee)):
            if self._employee[i].to_pay() > maxx:
                maxx = self._employee[i].to_pay()
                index = i

        return self._employee[index].Name

    def __repr__(self):
        return str(self._employee)


if __name__ == '__main__':
    employee1 = Employee('johns', 600)
    employee2 = Employee('Bryan', 350, 100)
    employee3 = Employee('Drew', 950, 100)
    company = Company([employee1, employee2, employee3])
    company.give_everybody_bonus(500)
    print(f'employee with max salary: {company.name_max_salary()}')
    print(f'total to pay: {company.total_to_pay()}')
