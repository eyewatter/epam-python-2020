<H1>Task #1 </H1>

![Src](1.jpg)
![Src](2.jpg)


<H1>Solution</H1>

<H4>[link to code](1.py)</H4>

<H4>Output</H4>

```diff 
try to insert one more: False
max area index: 2
min perimeter index: 1
number of squares: 1
```


<H1>Task #2 </H1>

![Src](3.jpg)
![Src](4.jpg)

<H1>Solution</H1>

<H4>[link to code](2.py)</H4>

<H4>Output</H4>

```diff 
employee with max salary: Drew
total to pay: 3400
```
