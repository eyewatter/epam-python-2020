def to_binary(number: int):
    b_number = ''
    summ = 0
    while number > 0:
        number, digit = divmod(number, 2)
        if digit == 1:
            summ += 1

        b_number += str(digit)

    return b_number[::-1], summ


if __name__ == '__main__':
    try:
        number = int(input('please, enter the number '))
        sequence, summ = to_binary(number)
        print(f'number is {number}, binary representation is {sequence}, sum is {summ}')

    except ValueError:
        print('not a number')
