def transpose_by_comprehension(matrix):
    t_matrix = [[matrix[i][j] for i in range(len(matrix))] for j in range(len(matrix[0]))]
    return t_matrix


def transpose_by_loop(matrix):
    t_matrix = []
    for i in range(len(matrix[0])):
        row = []
        for j in range(len(matrix)):
            row.append(0)
        t_matrix.append(row)

    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            t_matrix[j][i] = matrix[i][j]
    return t_matrix


if __name__ == '__main__':
    matrix = [[1, 2, 3],
              [4, 5, 6]]
    print(matrix)
    print(transpose_by_loop(matrix))
    print(transpose_by_comprehension(matrix))
