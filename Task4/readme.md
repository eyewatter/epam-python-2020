<H1>Task #1 </H1>

 Write the script that can transpose any matrix.

<H1>Solution</H1>

<H4>[link to code](1.py)</H4>

<H4>Output</H4>

```diff 
[[1, 2, 3], [4, 5, 6]]
[[1, 4], [2, 5], [3, 6]]
[[1, 4], [2, 5], [3, 6]]
```


<H1>Task #2 </H1>

Calculate the factorial for positive number n

<H1>Solution</H1>

<H4>[link to code](2.py)</H4>

<H4>Output</H4>

```diff 
input a number 5
factorial of 5 is 120
```


<H1>Task #3 </H1>

For a positive integer n calculate the result value, 
which is equal to the sum of the first n Fibonacci numbers.

<H4>[link to code](3.py)</H4>

<H4>Output</H4>

```diff 
please, enter the number 5
fibonacci sequence is [0, 1, 1, 2, 3]
sum of element if Fibonacci sequence is 7
```

<H1>Task #4 </H1>

For a positive integer n calculate the result value, 
which is equal to the sum of the '1' in the binary representation of n.

<H4>[link to code](4.py)</H4>

<H4>Output</H4>

```diff 
please, enter the number 120
number is 120, binary representation is 1111000, sum is 4
```

