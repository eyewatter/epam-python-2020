def factorial(number: int) -> int:
    return 1 if number < 1 else number * factorial(number - 1)


if __name__ == '__main__':
    try:
        number = int(input('input a number '))
        print(f'factorial of {number} is {factorial(number)}')
    except ValueError:
        print('not a number')
