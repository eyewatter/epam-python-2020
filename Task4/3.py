def fibonacci(number: int):
    accumulator = [0, 1]
    for i in range(2, number):
        accumulator.append(accumulator[i - 1] + accumulator[i - 2])
    return accumulator, sum(accumulator)


if __name__ == '__main__':
    try:
        number = int(input('please, enter the number '))
        sequence, summ = fibonacci(number)
        print(f'fibonacci sequence is {sequence}')
        print(f'sum of element if Fibonacci sequence is {summ}')
    except ValueError:
        print('not a number')
