def calculate_triangle_area(a, b, c):
    try:
        sp = float((a + b + c) / 2)
        area = (sp * (sp - a) * (sp - b) * (sp - c)) ** 0.5
        return area
    except ValueError:
        print("Check data")


calculated_area = calculate_triangle_area(4.5, 5.9, 9)
print("Triangle area with sides a = 4.5, b = 5.9 and c = 9 is:\t ",
      round(calculated_area, 2))
